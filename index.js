// console.log("Hello world!");


//  [SECTION] Assignment Operator
	// Basic Assignment Operator (=)
		// The assignment operator assigns the value of the right hand operand to a variable 
		let assignmentNumber = 8;
		console.log("The value of the assignmentNumber: " + assignmentNumber);
//  [SECTION] Arithmetic Operations

	let x = 200;
	let y = 18;

	// Addition

	let sum = x + y;
	console.log("Result of addition operator: " + sum);


	// Subtraction

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	// Multiplication

	let product = x * y;
	console.log("Result of product operator: " + product);

	// Division

	let quotient = x / y;
	console.log("Result of quotient operator: " + quotient);

	// Modulo is used to get remainder 
	let modulo = x % y;
	console.log("Result of modulo operator: " + modulo);

// Continuation of assignment operator

// Addition Assignment Operator
	
	// current value of assignmentNumber is 8
	// assignmentNumber = assignmentNumber + 2; // long method
	// console.log(assignmentNumber);


	// short method 
	assignmentNumber +=2;
	console.log("Result of Addition assignment operator: " + assignmentNumber); // assignment value is 10

	// assignmentNumber = assignmentNumber - 3;
	// console.log(assignmentNumber);


// Subtraction Assignment Operator
	assignmentNumber -= 3;
    console.log("Result of Subtraction Assignment Operator: " + assignmentNumber);



// Multiplication Assignment Operator
    assignmentNumber *= 2;
    console.log("Result of Multiplication Assignment Operator: " + assignmentNumber);


// Division Assignment Operator
    assignmentNumber /= 2;
    console.log("Result of Division Assignment Operator: " + assignmentNumber);

// [SECTION] PEMDAS (Order of Operation)
// Multiple Operators and Paranthesis 

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

/* 
	The operations were done in the following order; 
	1. 3 * 4 = 12 | 1 + 2 - 12 / 5
	2. 12 / 5 = 2.4 | 1 + 2 - 2.4
	3. 1 + 2 = 3 | 3 - 2.4
	4. 3 - 2.4 = 0.6


*/

let pemdas = (1+(2-3)) * (4/5);
console.log("Result of pemdas operation: " + pemdas);


// hierarchy
// combinations of multiple arithmetic operators will follow pemdas rule

/*
	1. parenthesis
	2. exponent 
	3. multiplication or division
	4. addition or subtraction 

	NOTE: will also follow left to right rule 

*/



// [SECTION] Increment and Decrement
// Increment - pagdagdag 
// Decrement - pagbawas 

// Operators that add or subtract vallues by 1 an reassign the value of the variable where the increment (++)/decrement(--) was applied.

let z = 1;

// pre-increment
let increment = ++z;
console.log("Result of pre-increment: " + increment); // 2
console.log("Result of pre-increment of z: " + z); //2


// post-increment 
// z = z + 1
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment of z: " + z); 


// pre-decrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement); //2
console.log("Result of pre-decrement of z: " + z); //2 


// post-derement
decrement = z--;
console.log("Result of pre-decrement: " + decrement); // 2
console.log("Result of pre-decrement of z: " + z); // 1 


// [SECTION] Type Coercion
//  is the automatic conversion of values from one data type to another 

	// combination number and string data type will result to string
	let numA = 10; // number
	let numB = "12"; // string

	let coercion = numA+numB;
	console.log(coercion);
	console.log(typeof coercion);


	// combination of number and number will ressult to number 
	let numC = 16;
	let numD = 14;
	let noncoercion = numC + numD;
	console.log(noncoercion);
	console.log(typeof noncoercion);


	//  combination of number and boolean data type will result to number 
	let numX = 10;
	let numY = true;
		// boolean values are 
			// true = 1;
			// false = 0;

	coercion = numX + numY;
	console.log(coercion);
	console.log(typeof coercion);



// [SECTION] Comparison Operators 
// Comparison operatprs are use to evaluate and compate the left and right operands 
//  After evaluation, it returns a boolean value 

// equality operator (== also read as equal to)
// compares the value but not the data type
console.log("--------------");
console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == "1"); //true
console.log(0 == false); // true //false boolean value is also equal to zero


let juan = "juan";
console.log("juan" == "juan"); //true
console.log("juan" == "Juan"); // false // equality operator is strict with later casing
console.log(juan == "juan"); //true


// inequality operator (!= also read as not equal to)
console.log("--------------");
console.log(1 != 1); //false 
console.log(1 != 2); //true
console.log(1 != "1"); //false
console.log(0 != false); //false

console.log("juan" != "juan"); // false
console.log("juan" != "Juan"); // true
console.log(juan != "juan"); //false



// [SECTION] Relational Operators
// some comparison operators to check wether one value is greaer or less than the other value
// it will return a value of true or false 
console.log("--------------");
let a = 50;
let b = 65;

// GT or Greater than operator (>)
let isGreaterThan = a > b; //50 > 65 = false
console.log(isGreaterThan);


// LT or Less than operator (>)
let isLessThan = a < b; //50 < 65 = true
console.log(isLessThan);


// GTE or Greater Than or Equal (>=)
let isGTorEqual = a >= b; // 50 >= 65 = false
console.log(isGTorEqual);


// LTE or Greater Than or Equal (<=)
let isLTorEqual = a <= b; // 50 <= 65 = true
console.log(isLTorEqual);

// Forces Coercion
let num = 56;
let numstr = "30";
console.log(num > numstr); // true
// forced coercion to change string to number

console.log("--------------");
let str = "twenty";
console.log(num >= str);
// since the string is not numeric, the string will not be converted to a number or a.k.a NAN (not a number) 



// Logical AND Operator (&&)

// Logical or Operator
let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered;
console.log("result of logical AND Operator: " +allRequirementsMet);
// And operator requires all / both are true


// Logical OR Operator (||)
let someRequirementsMet = isLegalAge || isRegistered;
console.log("result of logical OR Operator: " + someRequirementsMet);
// OR Operator requires only 1 true;

// Logical Not Operator (!)
// Returns the opposite value 
let someRequirementsNotMet = !isLegalAge; //false
console.log("result of logical NOT Operator: " + someRequirementsNotMet);







// strict equality 
console.log(1 == "1"); //true








